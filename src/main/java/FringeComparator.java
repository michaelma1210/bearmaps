/**
 * Created by michaelma on 4/16/16.
 */
import java.util.Comparator;

public class FringeComparator implements Comparator<Node> {

    public int compare(Node nodeA, Node nodeB) {
        if (nodeA.getPriority() > nodeB.getPriority()) {
            return 1;
        }
        if (nodeA.getPriority() < nodeB.getPriority()) {
            return -1;
        } else {
            return 0;
        }
    }
}

