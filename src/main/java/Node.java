/**
 * Created by michaelma on 4/14/16.
 */
import java.util.HashSet;
import java.util.Set;

public class Node {
    private long id;
    private double lat;
    private double lon;
    private String name;
    private static double INFINITY = Double.POSITIVE_INFINITY;

    private double distance = INFINITY;
    private double priority = INFINITY;
    private Node prev;

    private Set<Connection> connectionSet;

    public Node(long id, double lat, double lon) {
        this.id = id;
        this.lat = lat;
        this.lon = lon;
        connectionSet = new HashSet<>();
    }

    public double getPriority() {
        return priority;
    }

    public void setPriority(double priority) {
        this.priority = priority;
    }

    public Node getPrev() {
        return prev;
    }




    public void setPrev(Node prev) {
        this.prev = prev;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public double getDistance() {
        return distance;
    }

    public long getId() {
        return id;
    }

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }

    public Set<Connection> getConnectionSet() {
        return connectionSet;
    }

    @Override
    public int hashCode() {
        return (int) id;
    }

    @Override
    public boolean equals(Object obj) {
        Node node = (Node) obj;
        if (this.id == node.id) {
            return true;
        }
        return false;
    }
}
