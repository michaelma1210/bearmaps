/**
 * Created by michaelma on 4/7/16.
 */

import java.util.ArrayList;
import java.awt.geom.Rectangle2D;


public class QuadTree {
    private QTreeNode root;
    ArrayList<QTreeNode> tilesInQuery;

    Rectangle2D.Double queryRect;
    Rectangle2D.Double tileRect;


    public QuadTree(QTreeNode root) {
        this.root = root;
        tilesInQuery = new ArrayList<QTreeNode>();

    }


    public void clearArray() {
        tilesInQuery.clear();
    }

    public void newTraverse(QTreeNode tile, QueryParams queryParams) {

        if (!intersectsTile(tile, queryParams)) {
            return;
        }
        if (intersectsTile(tile, queryParams) && !satisfiesDepthOrIsLeaf(tile, queryParams)) {
            newTraverse(tile.upperLeft, queryParams);
            newTraverse(tile.upperRight, queryParams);
            newTraverse(tile.lowerLeft, queryParams);
            newTraverse(tile.lowerRight, queryParams);
        }
        if (intersectsTile(tile, queryParams) && satisfiesDepthOrIsLeaf(tile, queryParams)) {
            tilesInQuery.add(tile);
        }
    }

    private boolean satisfiesDepthOrIsLeaf(QTreeNode tile, QueryParams queryParams) {
        double tileLrlon = tile.lrlon();
        double tileUllon = tile.ullon();
        double tileDpp = (tileLrlon - tileUllon) / 256;
        double queryDpp = queryParams.queryDpp();

        if (tileDpp <= queryDpp) {
            return true;
        }
        if (tile.upperLeft == null && tile.upperRight == null
                && tile.lowerLeft == null && tile.lowerRight == null) {
            return true;
        }
        return false;
    }

    private boolean intersectsTile(QTreeNode tile, QueryParams queryParams) {
        double tileLrlon = tile.lrlon();
        double tileUllon = tile.ullon();
        double tileLrlat = tile.lrlat();
        double tileUllat = tile.ullat();
        double tileWidth = tileLrlon - tileUllon;
        double tileHeight = tileUllat - tileLrlat;
        tileRect = new Rectangle2D.Double(tile.ullon(), tile.lrlat(), tileWidth, tileHeight);
        queryRect = queryParams.queryRect();
        if (tileRect.intersects(queryRect)) {
            return true;
        }
        return false;

    }



    public ArrayList<QTreeNode> returnArray() {
        return tilesInQuery;
    }





}
