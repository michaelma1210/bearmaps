/**
 * Created by michaelma on 4/12/16.
 */
import java.util.Comparator;

public class TileComparator implements Comparator<QTreeNode> {

    public int compare(QTreeNode tileA, QTreeNode tileB) {
        if (tileA.ullat() > tileB.ullat()) {
            return -1;
        }
        if (tileA.ullat() < tileB.ullat()) {
            return 1;
        }
        if (tileA.ullat() == tileB.ullat()) {
            if (tileA.ullon() > tileB.ullon()) {
                return 1;
            }
            if (tileA.ullon() < tileB.ullon()) {
                return -1;
            }
        }
        return 0;


    }
}
