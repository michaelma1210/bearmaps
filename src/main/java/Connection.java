/**
 * Created by michaelma on 4/14/16.
 */
public class Connection {
    private Node nodeA;
    private Node nodeB;

    public Connection(Node nodeA, Node nodeB) {
        this.nodeA = nodeA;
        this.nodeB = nodeB;
    }

    public Node getNodeA() {
        return nodeA;
    }

    public Node getNodeB() {
        return nodeB;
    }
}
