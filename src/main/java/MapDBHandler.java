import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;


/**
 *  Parses OSM XML files using an XML SAX parser. Used to construct the graph of roads for
 *  pathfinding, under some constraints.
 *  See OSM documentation on
 *  <a href="http://wiki.openstreetmap.org/wiki/Key:highway">the highway tag</a>,
 *  <a href="http://wiki.openstreetmap.org/wiki/Way">the way XML element</a>,
 *  <a href="http://wiki.openstreetmap.org/wiki/Node">the node XML element</a>,
 *  and the java
 *  <a href="https://docs.oracle.com/javase/tutorial/jaxp/sax/parsing.html">SAX parser tutorial</a>.
 *  @author Alan Yao
 */
public class MapDBHandler extends DefaultHandler {
    private static double INFINITY = Double.POSITIVE_INFINITY;
    private static HashMap<String, String> cleanWords;


    boolean hasHighwayTag = false;
    /**
     * Only allow for non-service roads; this prevents going on pedestrian streets as much as
     * possible. Note that in Berkeley, many of the campus roads are tagged as motor vehicle
     * roads, but in practice we walk all over them with such impunity that we forget cars can
     * actually drive on them.
     */
    private static final Set<String> ALLOWED_HIGHWAY_TYPES = new HashSet<>(Arrays.asList
            ("motorway", "trunk", "primary", "secondary", "tertiary", "unclassified",
                    "residential", "living_street", "motorway_link", "trunk_link", "primary_link",
                    "secondary_link", "tertiary_link"));
    private String activeState = "";
    private final GraphDB g;

    public MapDBHandler(GraphDB g) {
        this.g = g;



    }


    /**
     * Called at the beginning of an element. Typically, you will want to handle each element in
     * here, and you may want to track the parent element.
     * @param uri The Namespace URI, or the empty string if the element has no Namespace URI or
     *            if Namespace processing is not being performed.
     * @param localName The local name (without prefix), or the empty string if Namespace
     *                  processing is not being performed.
     * @param qName The qualified name (with prefix), or the empty string if qualified names are
     *              not available. This tells us which element we're looking at.
     * @param attributes The attributes attached to the element. If there are no attributes, it
     *                   shall be an empty Attributes object.
     * @throws SAXException Any SAX exception, possibly wrapping another exception.
     * @see Attributes
     */
    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes)
            throws SAXException {
        /* Some example code on how you might begin to parse XML files. */
        if (qName.equals("node")) {
            activeState = "node";
            long id = Long.valueOf(attributes.getValue("id"));
            double lat = Double.valueOf(attributes.getValue("lat"));
            double lon = Double.valueOf(attributes.getValue("lon"));
            Node currNode = new Node(id, lat, lon);
            g.getNodeMap().put(id, currNode);

            g.getNodesForGetLoc().add(0, currNode);

        } else if (qName.equals("way")) {
            activeState = "way";
        } else if (activeState.equals("way") && qName.equals("tag")) {
            String k = attributes.getValue("k");
            String v = attributes.getValue("v");
            if (k.equals("highway") && ALLOWED_HIGHWAY_TYPES.contains(v)) {
                hasHighwayTag = true;

            }
        } else if (activeState.equals("way") && qName.equals("nd")) {
            long currNodeId = Long.valueOf(attributes.getValue("ref"));
            Node currNode = g.getNodeMap().get(currNodeId);
            g.getWayNodesList().add(currNode);
        } else if (activeState.equals("node") && qName.equals("tag") && attributes.getValue("k")
                .equals("name")) {
            String actualStr = attributes.getValue("v");
            String cleanedStr = g.cleanString(actualStr);

            g.getTrie().add(actualStr, cleanedStr);
            g.getCleanToActual().put(cleanedStr, actualStr);

            Node nodeForStr = g.getNodesForGetLoc().get(0);

            nodeForStr.setName(actualStr);

            if (!g.getMapForGetLoc().containsKey(cleanedStr)) {
                LinkedList<Node> newLinkedList = new LinkedList();
                newLinkedList.addFirst(nodeForStr);
                g.getMapForGetLoc().put(cleanedStr, newLinkedList);
            } else {
                LinkedList currLinkedList = g.getMapForGetLoc().get(cleanedStr);
                currLinkedList.add(nodeForStr);
            }
        }
    }

    /**
     * Receive notification of the end of an element. You may want to take specific terminating
     * actions here, like finalizing vertices or edges found.
     * @param uri The Namespace URI, or the empty string if the element has no Namespace URI or
     *            if Namespace processing is not being performed.
     * @param localName The local name (without prefix), or the empty string if Namespace
     *                  processing is not being performed.
     * @param qName The qualified name (with prefix), or the empty string if qualified names are
     *              not available.
     * @throws SAXException  Any SAX exception, possibly wrapping another exception.
     */
    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equals("way")) {
//            System.out.println("Finishing a way...");
            /*store element into database */
            /*if not highway, discard list you've accumulated */
            int size = g.getWayNodesList().size();
            int index = 0;
            if (hasHighwayTag) {
                if (size == 0) {
                    return;
                }
                while (index < size - 1) {
                    Node current = g.getWayNodesList().get(index);
                    Node next = g.getWayNodesList().get(index + 1);
                    Connection con = new Connection(current, next);
                    current.getConnectionSet().add(con);
                    next.getConnectionSet().add(con);
                    index += 1;
                }
            }

            g.getWayNodesList().clear();
            hasHighwayTag = false;
        }
    }


}
