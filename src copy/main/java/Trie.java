/**
 * Created by michaelma on 4/18/16.
 */
/* Implementation of Trie class inspired by LeetCode:
http://www.programcreek.com/2014/05/leetcode-implement-trie-prefix-tree-java/ */

import java.util.HashMap;
import java.util.Map;

public class Trie {
    private TrieNode root;

    public Trie() {
        root = new TrieNode();
    }


    public TrieNode getRoot() {
        return root;
    }

    public void add(String actualPhrase, String phrase) {
        HashMap<Character, TrieNode> children = root.getChildren();

        int index = 0;
        int stopPoint = phrase.length();
        while (index < stopPoint) {
            char character = phrase.charAt(index);
            TrieNode newTrieNode;

            if (children.containsKey(character)) {
                newTrieNode = children.get(character);
                newTrieNode.getSetOfChildren().add(actualPhrase);
            } else {
                newTrieNode = new TrieNode(character);
                newTrieNode.getSetOfChildren().add(actualPhrase);
                children.put(character, newTrieNode);
            }
            children = newTrieNode.getChildren();

            if (index == phrase.length() - 1) {
                newTrieNode.setLeaf(true);
            }
            index += 1;
        }
    }

    public TrieNode searchForNode(String phrase) {
        Map<Character, TrieNode> children = root.getChildren();
        TrieNode nodeToReturn = null;
        int index = 0;
        int stopPoint = phrase.length();
        while (index < stopPoint) {
            char character = phrase.charAt(index);
            if (children.containsKey(character)) {
                nodeToReturn = children.get(character);
                children = nodeToReturn.getChildren();
            } else {
                return null;
            }
            index += 1;
        }
        return nodeToReturn;
    }



}
