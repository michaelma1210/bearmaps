import org.xml.sax.SAXException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.util.LinkedList;


/**
 * Wraps the parsing functionality of the MapDBHandler as an example.
 * You may choose to add to the functionality of this class if you wish.
 * @author Alan Yao
 */
public class GraphDB {
    /**
     * Example constructor shows how to create and start an XML parser.
     * @param db_path Path to the XML file to be parsed.
     */

    private HashMap<Long, Node> nodeMap;
    private HashSet<Connection> connectionSet;
    private ArrayList<Node> wayNodesList;
    private ArrayList<Node> nodesToRemove;
    private Trie trie;
    private HashMap<String, String> cleanToActual;
    private ArrayList<Node> nodesForGetLoc;
    private HashMap<String, LinkedList> mapForGetLoc;

    public GraphDB(String dbPath) {
        nodeMap = new HashMap<>();
        connectionSet = new HashSet<>();
        wayNodesList = new ArrayList<>();
        trie = new Trie();
        cleanToActual = new HashMap<>();
        nodesForGetLoc = new ArrayList<>();
        mapForGetLoc = new HashMap<>();

        try {
            File inputFile = new File(dbPath);
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();
            MapDBHandler maphandler = new MapDBHandler(this);
            saxParser.parse(inputFile, maphandler);


        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }

        clean();

        nodesToRemove = new ArrayList<>();
        for (Node currNode: getNodeMap().values()) {
            if (currNode.getConnectionSet().isEmpty()) {
                nodesToRemove.add(currNode);
            }
        }
        for (Node item: nodesToRemove) {
            getNodeMap().remove(item.getId());
        }




    }

    public HashMap<Long, Node> getNodeMap() {
        return nodeMap;
    }

    public HashSet<Connection> getConnectionSet() {
        return connectionSet;
    }

    public ArrayList<Node> getWayNodesList() {
        return wayNodesList;
    }

    public Trie getTrie() {
        return trie;
    }

    public HashMap<String, String> getCleanToActual() {
        return cleanToActual;
    }

    public ArrayList<Node> getNodesForGetLoc() {
        return nodesForGetLoc;
    }

    public HashMap<String, LinkedList> getMapForGetLoc() {
        return mapForGetLoc;
    }



    /**
     * Helper to process strings into their "cleaned" form, ignoring punctuation and capitalization.
     * @param s Input string.
     * @return Cleaned string.
     */
    static String cleanString(String s) {
        return s.replaceAll("[^a-zA-Z ]", "").toLowerCase();
    }

    /**
     *  Remove nodes with no connections from the graph.
     *  While this does not guarantee that any two nodes in the remaining graph are connected,
     *  we can reasonably assume this since typically roads are connected.
     */
    private void clean() {
    }
}
