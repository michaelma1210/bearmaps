/**
 * Created by michaelma on 4/16/16.
 */
import java.util.Comparator;

public class FringeComparator implements Comparator<Node> {

    public int compare(Node nodeA, Node nodeB) {
        if (nodeA.getPriority() > nodeB.getPriority()) {
            return 1;
        }
        if (nodeA.getPriority() < nodeB.getPriority()) {
            return -1;
        } else {
            return 0;
        }
    }
}
//        if (nodeA.ullat() > tileB.ullat()) {
//            return -1;
//        }
//        if (tileA.ullat() < tileB.ullat()) {
//            return 1;
//        }
//        if (tileA.ullat() == tileB.ullat()) {
//            if (tileA.ullon() > tileB.ullon()) {
//                return 1;
//            }
//            if (tileA.ullon() < tileB.ullon()) {
//                return -1;
//            }
//        }
//        return 0;

