import java.util.HashMap;
import java.util.LinkedList;

/**
 * Created by michaelma on 4/18/16.
 */
public class TrieNode {
    HashMap<Character, TrieNode> children;
    boolean isLeaf = false;
    char character;
    LinkedList<String> setOfChildren;

    public TrieNode() {
        children = new HashMap<>();
        setOfChildren = new LinkedList<>();


    }

    public TrieNode(char character) {
        this.character = character;
        children = new HashMap<>();
        setOfChildren = new LinkedList<>();

    }

    public LinkedList<String> getSetOfChildren() {
        return setOfChildren;
    }


    public HashMap<Character, TrieNode> getChildren() {
        return children;
    }

    public boolean isLeaf() {
        return isLeaf;
    }

    public char getCharacter() {
        return character;
    }


    public void setLeaf(boolean leaf) {
        isLeaf = leaf;
    }

}
