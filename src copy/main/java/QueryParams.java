/**
 * Created by michaelma on 4/11/16.
 */
import java.awt.geom.Rectangle2D;

public class QueryParams {
    private double queryUllat;
    private double queryUllon;
    private double queryLrlat;
    private double queryLrlon;
    private double queryWidth;
    private double queryHeight;

    public QueryParams(double queryUllat, double queryUllon, double queryLrlat,
                       double queryLrlon, double queryWidth, double queryHeight) {
        this.queryUllat = queryUllat;
        this.queryUllon = queryUllon;
        this.queryLrlat = queryLrlat;
        this.queryLrlon = queryLrlon;
        this.queryWidth = queryWidth;
        this.queryHeight = queryHeight;
    }
    public Rectangle2D.Double queryRect() {
//    public mapRectangle queryRect() {
        double x = queryUllon;
        double y = queryLrlat;
        double width = queryLrlon - queryUllon;
        double height = queryUllat - queryLrlat;

//        return new mapRectangle(queryLrlat, queryUllon, queryLrlon, queryUllat);
        return new Rectangle2D.Double(x, y, width, height);
    }

    public double queryDpp() {
        return (queryLrlon - queryUllon) / queryWidth;
    }

    public double queryHeight() {
        return queryHeight;
    }

}
