import java.io.File;

/**
 * Created by michaelma on 4/7/16.
 */
public class QTreeNode {
    private String img;
    private double ullat;
    private double ullon;
    private double lrlat;
    private double lrlon;
    QTreeNode upperLeft;
    QTreeNode upperRight;
    QTreeNode lowerLeft;
    QTreeNode lowerRight;

    public QTreeNode(String img, double ullat, double ullon, double lrlat, double lrlon) {
        this.img = img;
        this.ullat = ullat;
        this.ullon = ullon;
        this.lrlat = lrlat;
        this.lrlon = lrlon;
        upperLeft = createUpperLeft();
        upperRight = createUpperRight();
        lowerLeft = createLowerLeft();
        lowerRight = createLowerRight();

    }

    public String img() {
        return img;
    }
    public double ullat() {
        return ullat;
    }

    public double ullon() {
        return ullon;
    }

    public double lrlat() {
        return lrlat;
    }

    public double lrlon() {
        return lrlon;
    }


    public QTreeNode createUpperLeft() {
        String newImgName;
        if (img.equals("root")) {
            newImgName = "1";
        } else {
            newImgName = img + "1";
        }
        double newUllat = ullat;
        double newUllon = ullon;
        double newLrlat = (ullat + lrlat) / 2;
        double newLrlon = (ullon + lrlon) / 2;
        if (checkIfFileExists(newImgName)) {
            return new QTreeNode(newImgName, newUllat, newUllon, newLrlat, newLrlon);
        } else {
            return null;
        }
    }

    public QTreeNode createUpperRight() {
        String newImgName;
        if (img.equals("root")) {
            newImgName = "2";
        } else {
            newImgName = img + "2";
        }
        double newUllat = ullat;
        double newUllon = (ullon + lrlon) / 2;
        double newLrlat = (ullat + lrlat) / 2;
        double newLrlon = lrlon;
        if (checkIfFileExists(newImgName)) {
            return new QTreeNode(newImgName, newUllat, newUllon, newLrlat, newLrlon);
        } else {
            return null;
        }

    }

    public QTreeNode createLowerLeft() {
        String newImgName;
        if (img.equals("root")) {
            newImgName = "3";
        } else {
            newImgName = img + "3";
        }
        double newUllat = (ullat + lrlat) / 2;
        double newUllon = ullon;
        double newLrlat = lrlat;
        double newLrlon = (ullon + lrlon) / 2;
        if (checkIfFileExists(newImgName)) {
            return new QTreeNode(newImgName, newUllat, newUllon, newLrlat, newLrlon);
        } else {
            return null;
        }

    }

    public QTreeNode createLowerRight() {
        String newImgName;
        if (img.equals("root")) {
            newImgName = "4";
        } else {
            newImgName = img + "4";
        }
        double newUllat = (ullat + lrlat) / 2;
        double newUllon = (ullon + lrlon) / 2;
        double newLrlat = lrlat;
        double newLrlon = lrlon;
        if (checkIfFileExists(newImgName)) {
            return new QTreeNode(newImgName, newUllat, newUllon, newLrlat, newLrlon);
        } else {
            return null;
        }

    }

    private boolean checkIfFileExists(String imgName) {
        File f = new File("img/" + imgName + ".png");
        if (f.exists() && !f.isDirectory()) {
            return true;
        }
        return false;
    }

}

//    public void entireTree(){
//        if(this.upperLeft != null){
//            System.out.println(this.upperLeft.img);
//            this.upperLeft.entireTree();
//        }
//        if(this.upperRight != null){
//            System.out.println(this.upperRight.img);
//            this.upperRight.entireTree();
//        }
//        if(this.lowerLeft != null){
//            System.out.println(this.lowerLeft.img);
//            this.lowerLeft.entireTree();
//        }
//        if(this.lowerRight != null){
//            System.out.println(this.lowerRight.img);
//            this.lowerRight.entireTree();
//        }
//    }










